#include <string.h>
#include <jansson.h>
#include <curl/curl.h>
#include <ulfius.h>
#include "csapp.h"
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>
#include <sched.h> 

#include <string.h>

#define PORT 8080
#define ARCHIVOS "/archivos"
#define DISPOSITIVOS "/dispositivos"
#define LISTAR 10
#define BORRAR 11
#define CREAR 12
#define ERROR -1
#define OK    200
#define NOT_FOUND 404
#define INTERNAL_SERVER_ERROR 500
#define BAD_REQUEST 400
#define REQUEST_TIMEOUT 408

extern char* monitor_host;

extern char* monitor_port;

int identificar_solicitud(json_t * json_recibido);


int is_object(json_t * json);


int node_exists(json_t * json, char * node);


int listar_dispositivos(const struct _u_request * request, struct _u_response * response, void * user_data);


int listar_archivos(const struct _u_request * request, struct _u_response * response);


int borrar_archivo(const struct _u_request * request, struct _u_response * response) ;


int crear_archivo(const struct _u_request * request, struct _u_response * response); 


int archivos_service (const struct _u_request * request, struct _u_response * response, void * user_data);

int test_monitor_conexion(char *monitorHost, char * monitorPort);


