
#include <libudev.h>
#include <mntent.h>
#include "../include/csapp.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <jansson.h>
#include <mntent.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/resource.h>

#define TRUE 1
#define FALSE 0
typedef unsigned char BOOL;


struct udev_device* obtener_hijo(struct udev* udev, struct udev_device* padre, const char* subsistema);
const char * listar_disp_alm_masivo(struct udev* udev);
void monitor(int connfd);
const char * listar_archivos(char* node);
const char * crear_archivo(const char * nodo, const char * nombre, const char * contenido);
const char * remover_archivo(const char * nodo, const char * nombre);
char * get_dir(char* node);
int get_size(char * filedir);
void *thread(void *vargp);
void daemonize(const char *cmd);
static BOOL CONTINUA;

sem_t mutex;

int main(int argc, char *argv[]){
	

	int listenfd, *connfdp;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *port, *haddrp;
	pthread_t tid;
	
	CONTINUA = TRUE;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	daemonize(argv[0]);
	
	listenfd = Open_listenfd(port);
	
	Sem_init(&mutex, 0, 1);
	

	while(CONTINUA)
	{	
	
		connfdp = Malloc(sizeof(int));
		*connfdp = Accept(listenfd, (SA *) &clientaddr, &clientlen);
		Pthread_create(&tid, NULL, thread, connfdp);
		
	}

}

/*Se genera un nuevo hilo y se llama a la funcion monitor().*/
void *thread(void *vargp){

	int connfd = *((int *)vargp);
	Pthread_detach(pthread_self());
	Free(vargp);
	monitor(connfd);
	Close(connfd);
	return NULL;
}

/*En esta funcion se analiza la solicitud enviada por el usuario a travez del webserver y se realiza la operacion correspondiente. */
void monitor(int connfd){

	char buf[MAXLINE];
	size_t n;
	rio_t rio;
	pid_t pid;
	const char *respuesta;
	char *peticion;
	json_t * json_peticion;
	json_t *solicitud;
	json_t *nodo;
	json_t *nombre;
	json_t *contenido;
	json_error_t error;
	const char* str_solicitud;
	const char* str_nodo;
	const char* str_nombre;
	const char* str_contenido;
	struct udev *udev;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0){
		peticion = strtok(buf,"\n");
		json_peticion =json_loads(peticion,0,&error);		
		solicitud = json_object_get(json_peticion, "solicitud");
		str_solicitud = json_string_value(solicitud);

		
		if(strcmp(str_solicitud,"dispositivos") == 0){
			udev = udev_new();
			if (!udev) {
				exit(1);
			}
			P(&mutex);
			respuesta = listar_disp_alm_masivo(udev);
			udev_device_unref(udev);
			V(&mutex);				
		
		}else if(strcmp(str_solicitud, "listar") == 0){
			
			nodo = json_object_get(json_peticion, "nodo");
			str_nodo = json_string_value(nodo);
			P(&mutex);
			respuesta = listar_archivos(str_nodo);
			V(&mutex);

		}else if(strcmp(str_solicitud, "crear") == 0){

			nodo = json_object_get(json_peticion, "nodo");
			nombre = json_object_get(json_peticion, "nombre");
			contenido = json_object_get(json_peticion, "contenido");
			str_nodo = json_string_value(nodo);
			str_nombre = json_string_value(nombre);
			str_contenido = json_string_value(contenido);
			P(&mutex);
			respuesta = crear_archivo(str_nodo, str_nombre, str_contenido);
			V(&mutex);				

		}else if(strcmp(str_solicitud, "borrar") == 0){

			nodo = json_object_get(json_peticion, "nodo");
			nombre = json_object_get(json_peticion, "nombre");
			str_nodo = json_string_value(nodo);
			str_nombre = json_string_value(nombre);
			P(&mutex);
			respuesta = remover_archivo(str_nodo, str_nombre);
			V(&mutex);				
		}else{
			respuesta = "Error";
		}
		Rio_writen(connfd, respuesta, strlen(respuesta));
	}

}

/*Lista los archivos contenidos en un dispositivo USB, el dispositivo es indicado por medio de su nodo.*/
const char * listar_archivos(char* nodo){

	char* sendnodo= malloc(strlen(nodo));
	strcpy(sendnodo, nodo);
	const char* dir = get_dir(sendnodo);
	DIR *odir;
	json_t * json_respuesta = NULL;
	json_respuesta = json_object();          
	json_t * dispositivosjson = json_array();
	
	struct dirent *ent;
	if(dir != NULL){

		if ((odir = opendir (dir)) != NULL) {
			
		  	while ((ent = readdir (odir)) != NULL) {

				if (ent->d_type == DT_REG){
					char peso[20];
					json_t * contenido = json_object();
					json_object_set_new(contenido, "nombre",  json_string(ent->d_name));
					dir = get_dir(nodo);	
					strcat(dir, "/");
					strcat(dir, ent->d_name);
					sprintf(peso, "%d Bytes", get_size(dir));
					json_object_set_new( contenido, "size", json_string(peso));
					
					json_array_append( dispositivosjson , contenido);
				}

		  	}
						
			closedir (odir);
			json_object_set_new( json_respuesta, "dispositivos", dispositivosjson);
			json_object_set_new( json_respuesta, "status", json_integer( 0 ));
			json_object_set_new( json_respuesta, "str_error", json_string(" "));
		  	
		}else{
			json_object_set_new( json_respuesta, "dispositivos", dispositivosjson);
	  		json_object_set_new( json_respuesta, "status", json_integer( -1 ));
			json_object_set_new( json_respuesta, "str_error", json_string(" Directory not found "));
		} 
	}else {
		json_object_set_new( json_respuesta, "dispositivos", dispositivosjson);
	  	json_object_set_new( json_respuesta, "status", json_integer( -1 ));
		json_object_set_new( json_respuesta, "str_error", json_string(" Directory not found "));
	}
	
	char *s = json_dumps(json_respuesta, 0);
	strcat(s, "\n");
	json_decref(json_respuesta);
	json_decref(dispositivosjson);
	return s;


}


int get_size(char * filedir){
	
	struct stat st;
	stat(filedir, &st);
	return st.st_size;	
}

/*Crea un archivo en el dispositivo USB indicado por el nodo, para esto se necesita el nombre mas la extension del archivo, ademas despues de creadp se escribe en el archivo el contenido que el usuario desee.*/
const char * crear_archivo(const char * nodo, const char * nombre, const char * contenido){
	
	char* sendnodo= malloc(strlen(nodo));
	strcpy(sendnodo, nodo);
	const char* dir = get_dir(sendnodo);
	char *contenidocompleto = malloc(strlen(contenido)+1);
	
	if(dir != NULL){
		char *filedir = malloc(strlen(dir) + strlen(nombre));
		filedir[0]='\0';
		contenidocompleto[0]='\n';
		strcat(filedir, dir);	
		strcat(filedir, "/");
		strcat(filedir, nombre);
		strcat(contenidocompleto, contenido);
		strcat(contenidocompleto, "\n");	
		FILE *fp = fopen(filedir, "a+");
		if(fp){
			fputs(contenido, fp);
			fclose(fp);
			return "200\n";
		}else{

			return "500\n";
		}	
	}else{

		return "500\n";	
	}
}

/*Remueve un archivo en el dispositivo USB indicado por el nodo, el archivo es identificado por medio de su nombre completo (incluido extension).*/
const char * remover_archivo(const char * nodo, const char * nombre){

	int ret;
	FILE *fp;
	char* sendnodo= malloc(strlen(nodo));
	strcpy(sendnodo, nodo);
	char* dir = get_dir(sendnodo);

	
	if(dir != NULL){
		char *filedir = malloc(strlen(dir) + strlen(nombre));
		filedir[0]='\0';
		strcat(filedir, dir);	
		strcat(filedir, "/");
		strcat(filedir, nombre);
		ret = remove(filedir);
		if(ret==0){
	
			return "200\n";
		}else{

			return "404\n";	
		}
	}else{

		return "404\n";
	}	
			
}

/*Funcion que retorna la direccion en el pc de un nodo en especifico.*/
char * get_dir(char* nodo){

	struct mntent *m;
	char *dir;
	FILE *f;
	char* actualnode = malloc( strlen(nodo)+ 1);
	actualnode[0]='\0';
	strcat(actualnode, nodo);
	strcat(actualnode, "1");
	f = setmntent(_PATH_MOUNTED, "r");
	while ((m = getmntent(f))) {
		if(strcmp(m->mnt_fsname, actualnode) == 0){
			
			dir = m->mnt_dir;
			endmntent(f);
			return dir;
		}
	}
	endmntent(f);
	return NULL;
}

/*Funcion usada en: listar_disp_alm_masivo(struct udev* udev). */
struct udev_device* obtener_hijo(struct udev* udev, struct udev_device* padre, const char* subsistema){

	struct udev_device* hijo = NULL;
	struct udev_enumerate *enumerar = udev_enumerate_new(udev);
	
	udev_enumerate_add_match_parent(enumerar, padre);
	udev_enumerate_add_match_subsystem(enumerar, subsistema);
	udev_enumerate_scan_devices(enumerar);

	struct udev_list_entry *dispositivos = udev_enumerate_get_list_entry(enumerar);
	struct udev_list_entry *entrada;

	udev_list_entry_foreach(entrada, dispositivos){
		const char *ruta = udev_list_entry_get_name(entrada);
		hijo = udev_device_new_from_syspath(udev, ruta);
		break;	
	}
		
	udev_enumerate_unref(enumerar);
	return hijo;
}

/*Lista los dispositivos de almacenamiento masivo conectados en los puertos USB.*/
const char * listar_disp_alm_masivo(struct udev* udev){
	
	struct udev_enumerate* enumerar = udev_enumerate_new(udev);
	
	udev_enumerate_add_match_subsystem(enumerar, "scsi");
	udev_enumerate_add_match_property(enumerar, "DEVTYPE", "scsi_device");
	udev_enumerate_scan_devices(enumerar);

	struct udev_list_entry *dispositivos = udev_enumerate_get_list_entry(enumerar);
	struct udev_list_entry *entrada;
	json_t * json_respuesta = NULL;
	json_respuesta = json_object();          
	json_t * dispositivosjson = json_array();

	int error = 0;

	udev_list_entry_foreach(entrada, dispositivos){
	
		const char* ruta = udev_list_entry_get_name(entrada);
		struct udev_device* scsi = udev_device_new_from_syspath(udev, ruta);

		struct udev_device* block = obtener_hijo(udev, scsi, "block");
		struct udev_device* scsi_disk = obtener_hijo(udev, scsi, "scsi_disk");

		struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(scsi, "usb", "usb_device");
		
		if(block && scsi_disk && usb){
			json_t * contenido = json_object();
			json_object_set_new( contenido, "nombre",  json_string(udev_device_get_sysname(block)));
			char* usbdata = malloc(sizeof(char)*50);
			usbdata[0]='\0';
			strcat(usbdata, udev_device_get_sysattr_value(usb, "idVendor"));
			strcat(usbdata, ":");
			strcat(usbdata, udev_device_get_sysattr_value(usb, "idProduct")); 			   
			json_object_set_new( contenido, "id",  json_string(usbdata)); 
			json_object_set_new( contenido, "Montaje", json_string(udev_device_get_syspath(block)));
			json_object_set_new( contenido, "Nodo", json_string(udev_device_get_devnode(block)));	

			json_array_append( dispositivosjson , contenido);
		}

		if(block){
			udev_device_unref(block);
		}
		if(scsi_disk){
			udev_device_unref(scsi_disk);	
		}
		
		udev_device_unref(scsi);
			
	}
	udev_device_unref(enumerar);

	json_object_set_new( json_respuesta, "dispositivos", dispositivosjson);
	json_object_set_new( json_respuesta, "status", json_integer( 0 ));
	json_object_set_new( json_respuesta, "str_error", json_string(" "));	

	

	char *s = json_dumps(json_respuesta, 0);
	strcat(s, "\n");
	return s;
}

void daemonize(const char *cmd) {
	int	i, fd0, fd1, fd2;
	pid_t	pid;
	struct rlimit	rl;


	/*
	 * Clear file creation mask.
	 */
	umask(0);

	/*
	 * Get maximum number of file descriptors.
	 */
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
		unix_error("can't get file limit");

	/*
	 * Become a session leader to lose controlling TTY.
	 */
	if ((pid = fork()) < 0)
		unix_error("can't fork");
	else if (pid != 0) /* parent */
		exit(0);
	setsid();



	/*
	 * Change the current working directory to the root so
	 * we won't prevent file systems from being unmounted.
	 */
	if (chdir("/") < 0)
		unix_error("can't change directory to /");

	/*
	 * Close all open file descriptors.
	 */
	if (rl.rlim_max == RLIM_INFINITY)
		rl.rlim_max = 1024;
	for (i = 0; i < rl.rlim_max; i++)
		close(i);

	/*
	 * Attach file descriptors 0, 1, and 2 to /dev/null.
	 */
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);

	/*
	 * Initialize the log file.
	 */
	openlog(cmd, LOG_CONS, LOG_DAEMON);
	if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d",
		  fd0, fd1, fd2);
		exit(1);
	}
}


